//
//  MGRMemePreViewController.h
//  Meme Generator
//
//  Created by Рома Дудка on 20.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meme.h"

@interface MGRMemePreViewController : UIViewController

@property (strong, nonatomic) Meme *selectedMeme;

@end
