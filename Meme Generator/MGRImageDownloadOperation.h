//
//  MGRImageDownloadOperation.h
//  Meme Generator
//
//  Created by Рома Дудка on 22.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MGRImageDownloadOperation : NSOperation

@property (nonatomic, readonly) NSError* error;
@property (nonatomic, strong) NSData* data;


- (id)initWithURL:(NSURL*)url;

@end
