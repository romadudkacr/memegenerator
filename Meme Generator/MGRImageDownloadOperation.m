//
//  MGRImageDownloadOperation.m
//  Meme Generator
//
//  Created by Рома Дудка on 22.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import "MGRImageDownloadOperation.h"

@interface MGRImageDownloadOperation ()

@property (nonatomic, strong) NSURL* url;
@property (nonatomic, strong) NSURLConnection* connection;
@property (nonatomic, strong) NSMutableData* buffer;
@property (nonatomic) long long int expectedContentLength;
@property (nonatomic, readwrite) NSError* error;
@property (nonatomic) BOOL isExecuting;
@property (nonatomic) BOOL isConcurrent;
@property (nonatomic) BOOL isFinished;

@end

@implementation MGRImageDownloadOperation
{
    BOOL runLoopDone;
}

- (id)initWithURL:(NSURL*)url
{
    self = [super init];
    if(self)
    {
        self.isExecuting = NO;
        self.isConcurrent = YES;
        self.isFinished = NO;
        self.url = url;
    }
    return self;
}

- (void)start
{
    NSURLRequest* request = [NSURLRequest requestWithURL:self.url];
    _isExecuting = YES;
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    
    NSPort* port = [NSPort port];
    NSRunLoop* rl = [NSRunLoop currentRunLoop];
    [rl addPort:port forMode:NSDefaultRunLoopMode];
    [_connection scheduleInRunLoop:rl forMode:NSDefaultRunLoopMode];
    [_connection start];
    [rl run];
}

#pragma mark NSURLConnectionDelegate

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.expectedContentLength = response.expectedContentLength;
    self.buffer = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.buffer appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return cachedResponse;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.data = self.buffer;
    self.buffer = nil;
    self.isExecuting = NO;
    self.isFinished = YES;
}

- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    self.error = error;
    self.isExecuting = NO;
    self.isFinished = YES;
}

- (void)setIsExecuting:(BOOL)isExecuting
{
    [self willChangeValueForKey:@"isExecuting"];
    _isExecuting = isExecuting;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)setIsFinished:(BOOL)isFinished
{
    [self willChangeValueForKey:@"isFinished"];
    _isFinished = isFinished;
    [self didChangeValueForKey:@"isFinished"];
}

- (void)cancel
{
    [super cancel];
    [self.connection cancel];
    self.isFinished = YES;
    self.isExecuting = NO;
}

@end
