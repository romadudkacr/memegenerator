//
//  MGRMemeCreateViewController.m
//  Meme Generator
//
//  Created by Рома Дудка on 20.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import "MGRMemeCreateViewController.h"
#import "MGRCreateMemePreViewController.h"
#import "MGRImageDownloadOperation.h"

@interface MGRMemeCreateViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) MGRImageDownloadOperation *downloadOperation;
@property (strong, nonatomic) UISegmentedControl *segControl;
@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) UIActivityIndicatorView *downloadIndicator;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *url;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *topTextField;
@property (weak, nonatomic) IBOutlet UITextField *bottomTextField;
@property (weak, nonatomic) IBOutlet UITextField *urlField;


- (IBAction)pickImage:(UIButton *)sender;


@end

@implementation MGRMemeCreateViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.segControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects: @"Gallery", @"URL", nil]];
    self.navigationItem.titleView = self.segControl;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                              style:UIBarButtonItemStyleBordered
                                                                             target:self
                                                                             action:@selector(createMemePreview:)];
    [self.segControl addTarget:self action:@selector(switchImageSource:) forControlEvents:UIControlEventValueChanged];
    self.topTextField.delegate = self;
    self.bottomTextField.delegate = self;
    self.urlField.delegate = self;
    self.titleTextField.delegate = self;
    [self.urlField setHidden:YES];
    self.segControl.selectedSegmentIndex = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)createMemePreview:(id)sender
{
    [self performSegueWithIdentifier:@"createMemePreView" sender:nil];
}

-(void)switchImageSource:(id)sender
{
    switch ([sender selectedSegmentIndex])
    {
        case 0:
        {
            [UIView animateWithDuration:3.0 animations:^{
                [self.urlField setHidden:YES];
            }];
            
            break;
        }
        case 1:
        {
            [UIView animateWithDuration:3.0 animations:^{
                [self.urlField setHidden:NO];
            }];
            
            break;
        }

        default:
            break;
    }
}

- (IBAction)pickImage:(UIButton *)sender
{
    switch ([self.segControl selectedSegmentIndex])
    {
        case 0:
        {
            [self showImagePicker];
            
            break;
        }
        case 1:
        {
            NSURL *url = [NSURL URLWithString:self.urlField.text];
            [self getImageFromURL:url];
            
            break;
        }
            
        default:
            break;
    }
    
}

- (void)showImagePicker
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    self.imagePickerController = imagePickerController;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (void)getImageFromURL:(NSURL *)url
{
    __weak typeof(self) weakSelf = self;
    self.downloadOperation = [[MGRImageDownloadOperation alloc] initWithURL:url];
    [self.downloadOperation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf.downloadOperation start];
    });
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"isFinished"])
    {
        UIImage *newImage = [UIImage imageWithData:[[object data] copy]];
        self.image = newImage;
        self.downloadOperation = nil;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                                  style:UIBarButtonItemStyleBordered
                                                                                 target:self
                                                                                 action:@selector(createMemePreview:)];
        [self.view setNeedsDisplay];
        [self.downloadIndicator stopAnimating];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - TextFieldDelegate 

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.urlField)
    {
        self.url = self.urlField.text;
    }
    [textField resignFirstResponder];
    
    return YES;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"createMemePreView"])
    {
        MGRCreateMemePreViewController *vc = [segue destinationViewController];
        switch ([self.segControl selectedSegmentIndex])
        {
            case 0:
            {
                vc.image = self.image;
                
                break;
            }
            case 1:
            {
                vc.image = self.image;
                
                break;
            }
        }
        vc.topText = self.topTextField.text;
        vc.bottomText = self.bottomTextField.text;
        vc.titleText = self.titleTextField.text;
    }
}

@end

