//
//  MGRCreateMemePreViewController.m
//  Meme Generator
//
//  Created by Рома Дудка on 21.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import "MGRCreateMemePreViewController.h"
#import "AppDelegate.h"
#import "Meme.h"

@interface MGRCreateMemePreViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) UILabel *topTextLabel;
@property (strong, nonatomic) UILabel *bottomTextLabel;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImage *myImage;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIAlertView *saveAlertView;

@end

@implementation MGRCreateMemePreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(saveButtonAction:)];
    self.myImage = [self imageWithImage:self.image
                          scaledToWidth:self.view.frame.size.width];
    self.imageView = [[UIImageView alloc] initWithImage:self.myImage];
    [self.imageView setFrame:CGRectMake(0, 0, self.myImage.size.width, self.myImage.size.height)];
    [self.view addSubview:self.imageView];
    self.imageView.center = self.view.center;
    [self.imageView addSubview:self.bottomTextLabel];
    [self.imageView addSubview:self.topTextLabel];
    [self.imageView setUserInteractionEnabled:YES];
    [self.view setContentMode:UIViewContentModeCenter];
}

- (void)saveButtonAction:(id)sender
{
    self.saveAlertView = [[UIAlertView alloc] initWithTitle:@"Share?"
                                                message:@"Whould you like to share the meme?"
                                               delegate:self
                                      cancelButtonTitle:@"No"
                                      otherButtonTitles:@"Yes", nil];
    [self.saveAlertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            [self saveMeme:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            break;
        }
        case 1:
        {
            [self saveMeme:nil];
            UIImage *newImage = [self createImage:self.imageView];
            UIActivityViewController *activity = [[UIActivityViewController alloc]
                                                  initWithActivityItems:@[newImage]
                                                  applicationActivities:nil];
            [self presentViewController:activity animated:YES completion:nil];
            
            break;
        }
    }
}

-(void)saveMeme:(id)sender
{
    UIImage *newImage = [self createImage:self.imageView];
    UIImage *newThumbnail = [self createThumbnailFromImage:newImage];
    NSDate *date = [NSDate date];
    Meme *newMeme = [NSEntityDescription insertNewObjectForEntityForName:@"Meme" inManagedObjectContext:self.managedObjectContext];
    [newMeme setValue:newImage forKey:@"image"];
    [newMeme setValue:self.titleText forKey:@"title"];
    [newMeme setValue:newThumbnail forKey:@"thumbnail"];
    [newMeme setValue:date forKey:@"dateCreated"];
    
    NSError *error = nil;
    if (![self.managedObjectContext save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(CGFloat)width
{
    CGFloat oldWidth = sourceImage.size.width;
    CGFloat scaleFactor = width / oldWidth;
    
    CGFloat newHeight = sourceImage.size.height * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UILabel *)bottomTextLabel
{
    if (!_bottomTextLabel)
    {
        _bottomTextLabel = [[UILabel alloc] init];
    }
    [_bottomTextLabel setFont:[UIFont fontWithName:@"Marker Felt" size:25]];
    [_bottomTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize labelSize = CGSizeMake(self.view.frame.size.width, 300);
    CGSize stringSize = [self.bottomText sizeWithFont:_bottomTextLabel.font constrainedToSize:labelSize lineBreakMode:_bottomTextLabel.lineBreakMode];
    [_bottomTextLabel setFrame:CGRectMake(self.view.frame.size.width / 2 - stringSize.width / 2,
                                          self.myImage.size.height - stringSize.height,
                                          stringSize.width, stringSize.height)];
    [_bottomTextLabel setNumberOfLines:0];
    [_bottomTextLabel setText:self.bottomText];
    [_bottomTextLabel setTextColor:[UIColor greenColor]];
    [_bottomTextLabel setUserInteractionEnabled:YES];
    [_bottomTextLabel setTextAlignment:NSTextAlignmentCenter];
    _bottomTextLabel.preferredMaxLayoutWidth = self.view.frame.size.width;
    
    return _bottomTextLabel;
}

- (UILabel *)topTextLabel
{
    if (!_topTextLabel)
    {
        _topTextLabel = [[UILabel alloc] init];
    }
    [_topTextLabel setFont:[UIFont fontWithName:@"Marker Felt" size:25]];
    [_topTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize labelSize = CGSizeMake(self.view.frame.size.width, 300);
    CGSize stringSize = [self.topText sizeWithFont:_topTextLabel.font constrainedToSize:labelSize lineBreakMode:_topTextLabel.lineBreakMode];
    [_topTextLabel setFrame:CGRectMake(self.view.frame.size.width / 2 - stringSize.width / 2, 0, stringSize.width, stringSize.height)];
    [_topTextLabel setNumberOfLines:0];
    [_topTextLabel setText:self.topText];
    [_topTextLabel setTextColor:[UIColor greenColor]];
    [_topTextLabel setUserInteractionEnabled:YES];
    [_topTextLabel setTextAlignment:NSTextAlignmentCenter];
    _topTextLabel.preferredMaxLayoutWidth = self.view.frame.size.width;
    
    return _topTextLabel;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(UIImage *)createImage:(UIImageView *)imgView
{
    UIGraphicsBeginImageContextWithOptions(imgView.bounds.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [imgView.layer renderInContext:context];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)createThumbnailFromImage:(UIImage *)image
{
    UIImage *originalImage = image;
    CGSize destinationSize = CGSizeMake(50, 50);
    
    UIGraphicsBeginImageContext(destinationSize);
    [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (!_managedObjectContext)
    {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        _managedObjectContext = [appDelegate managedObjectContext];
    }
    
    return _managedObjectContext;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
