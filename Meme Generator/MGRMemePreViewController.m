//
//  MGRMemePreViewController.m
//  Meme Generator
//
//  Created by Рома Дудка on 20.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import "MGRMemePreViewController.h"

@interface MGRMemePreViewController ()

@property (strong, nonatomic) UIImageView *imageView;

@end

@implementation MGRMemePreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = self.selectedMeme.title;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(shareMeme:)];
    
    self.view = self.imageView;
    self.imageView.image = self.selectedMeme.image;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)shareMeme:(id)sender
{
    UIActivityViewController *activity = [[UIActivityViewController alloc]
                                          initWithActivityItems:@[self.selectedMeme.image]
                                          applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];
}

- (UIImageView *)imageView
{
    if (!_imageView)
    {
        _imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    }
    
    return _imageView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
