//
//  Meme.h
//  Meme Generator
//
//  Created by Рома Дудка on 22.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Meme : NSManagedObject

@property (nonatomic, retain) id image;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) id thumbnail;
@property (nonatomic, retain) NSDate * dateCreated;

@end
