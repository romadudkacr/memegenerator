//
//  Meme.m
//  Meme Generator
//
//  Created by Рома Дудка on 22.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import "Meme.h"


@implementation Meme

@dynamic image;
@dynamic title;
@dynamic thumbnail;
@dynamic dateCreated;

@end
