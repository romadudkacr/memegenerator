//
//  MGRCreateMemePreViewController.h
//  Meme Generator
//
//  Created by Рома Дудка on 21.07.15.
//  Copyright (c) 2015 Рома Дудка. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGRCreateMemePreViewController : UIViewController

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *titleText;
@property (strong, nonatomic) NSString *topText;
@property (strong, nonatomic) NSString *bottomText;

@end
